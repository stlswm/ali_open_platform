<?php

namespace stlswm\AliOpenPlatform\DingRobot;

use Exception;

/**
 * @param string $token
 *
 * @return DingRobot
 * @throws Exception
 */
function NewDingRobot(string $token): DingRobot
{
    $dingRobot = new DingRobot();
    $dingRobot->setDefaultToken($token);
    $dingRobot->useToken('default');
    return $dingRobot;
}